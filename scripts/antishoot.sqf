if(!isDedicated) then
{
	firedNum = 0;
	waitUntil{!isnull player};
	[] spawn 
	{
		sleep (60 * 10);
		firedNum = 0;
	};
	
customEVH = player addEventHandler["Fired",
{
 _bullet = _this select 6;
 if(!local _bullet) exitWith {};
 if(player distance (getMarkerPos "respawn_west") > 300) exitWith {};
while{!isNull _bullet} do
{
	//_pos = getPos _bullet;
	if((getPos _bullet) distance (getMarkerPos "respawn_west") < 150) then
	{
		deleteVehicle _bullet;
		_bullet = ObjNull;
		firedNum = firedNum + 1;
		if(firedNum == 1) then
		{
			hint "This is your first warning, stop firing into the base.";
		};
		
		if(firedNum == 2) then
		{
			hint "Stop firing into the base, final warning. Your weapons have not been removed, do it once more and you will be automatically kicked from the server.";
			removeAllWeapons player;
			{player removeMagazine _x} foreach (magazines player);
			removeAllItems player;
		};
		
		if(firedNum == 3) then
		{
			[nil,nil,rHINT,format["%1 has been kicked for excessive shooting in the base.",name player]] call RE;
			sleep 0.1;
			endMission "loser";
		};
	};
};
}
];
};