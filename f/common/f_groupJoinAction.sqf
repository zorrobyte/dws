// F3 - Group join action

// ====================================================================================

private ["_targetGroup"];
_targetGroup = _this select 3;

// Join player to the target group
[player] joinSilent _targetGroup;