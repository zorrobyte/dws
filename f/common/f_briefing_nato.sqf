// F3 - Briefing

// ====================================================================================

// FACTION: NATO

// ====================================================================================

// TASKS
// The code below creates tasks. A (commented-out) sample task is included.

// _task1 = player createSimpleTask ["OBJ_1"];
// _task1 setSimpleTaskDescription ["IN DEPTH OBJECTIVE DESCRIPTION", "SHORT OBJECTIVE DESCRIPTION", "WAYPOINT TEXT"];
// _task1 setSimpleTaskDestination WAYPOINTLOCATION;
// _task1 setTaskState "Created";

// ====================================================================================

// NOTES: CREDITS
// The code below creates the administration sub-section of notes.

_cre = player createDiaryRecord ["diary", ["Credits","
<br/>
BTC for revive
Tonic for VAS
[KH]Jman for base layout
Focht for his Seize ground mission for the insurgency style markers
TAW_Tonic for scripting support, bulletshot base protection, VAS and keeping me up for days on end
MSO Team for the MSO framework
F3 for their mission framework
Task Force Blackjack for testing and suggestions
The Digital Army for testing and feedback
<br/><br/>
Created by TAW_Zorrobyte
"]];

// ====================================================================================

// NOTES: ADMINISTRATION
// The code below creates the administration sub-section of notes.

_adm = player createDiaryRecord ["diary", ["Administration","
<br/>
Although elements of heavy armor and attack choppers have been bogged down in carrier attacks in the Miditerranean Sea, NATO has secured a foothold at the Stratis Air Base with light choppers and vehicles.
"]];

// ====================================================================================

// NOTES: EXECUTION
// The code below creates the execution sub-section of notes.

_exe = player createDiaryRecord ["diary", ["Execution","
<br/>
COMMANDER'S INTENT
<br/>
Minimize NATO casualties by applying squad cohesion in the field. 
<br/><br/>
MOVEMENT PLAN
<br/>
Air assets are authorized for unurbanized LZs only while light vehicles are suggested for urban areas.
<br/><br/>
FIRE SUPPORT PLAN
<br/>
One AH-9 is available for CAS support. 
<br/><br/>
SPECIAL TASKS
<br/>
Intel reveals several high priority missions may be coming up in the near future.
"]];

// ====================================================================================

// NOTES: MISSION
// The code below creates the mission sub-section of notes.

_mis = player createDiaryRecord ["diary", ["Mission","
<br/>
Secure several high value locations within Stratis in effort to gain a foothold in preparation for assault on Altis.
"]];

// ====================================================================================

// NOTES: SITUATION
// The code below creates the situation sub-section of notes.

_sit = player createDiaryRecord ["diary", ["Situation","
<br/>
NATO must secure Stratis for continuing operations into Atlas.
<br/><br/>
ENEMY FORCES
<br/>
Iranian forces of an unknown number reporting to have armed light transport capability.
<br/><br/>
FRIENDLY FORCES
<br/>
Company of NATO units with chopper assets and light transport.
"]];

// ====================================================================================