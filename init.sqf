// ====================================================================================

// F3 - Process ParamsArray

// WARNING: DO NOT DISABLE THIS COMPONENT

f_processParamsArray = [] execVM "f\common\f_processParamsArray.sqf";

// ====================================================================================

// F3 - Disable Saving and Auto Saving


enableSaving [false, false];

// ====================================================================================

// F3 - Respawn INIT


f_respawnINIT = player addEventHandler ["killed", {_this execVM "init_onPlayerRespawn.sqf"}];

// ====================================================================================

// F3 - Mission Maker Teleport


// f_missionMakerTeleport = 0;
// [] execVM "f\common\f_missionMakerTeleport.sqf";

// ====================================================================================

// F3 - Briefing


[] execVM "briefing.sqf";

// ====================================================================================

// F3 - Mission Conditions Selector


[] execVM "f\common\f_setMissionConditions.sqf";

// ====================================================================================

// F3 - F3 Folk ARPS Group IDs


f_script_setGroupIDs = [] execVM "f\common\folk_setGroupIDs.sqf";

// ====================================================================================

// F3 - ShackTactical Fireteam Member Markers


[] execVM "f\common\ShackTac_setlocalFTMemberMarkers.sqf";

// ====================================================================================

// F3 - F3 Folk ARPS Group Markers


[] execVM "f\common\folk_setLocalGroupMarkers.sqf";

// ====================================================================================

// F3 - F3 Common Local Variables

// WARNING: DO NOT DISABLE THIS COMPONENT

f_script_setLocalVars = [] execVM "f\common\f_setLocalVars.sqf";

// ====================================================================================

// F3 - Multiplayer Ending Controller 


f_endSelected = -1;
[] execVM "f\common\f_mpEndSetUp.sqf";

// ====================================================================================

// F3 - Kegetys Spectator Script


[] execVM "f\common\f_spect\specta_init.sqf";

// ====================================================================================

// F3 - Automatic Body Removal


f_removeBodyDelay = 180;
f_doNotRemoveBodies = [];
[] execVM "f\common\f_addRemoveBodyEH.sqf";

// ====================================================================================

// F3 - Dynamic View Distance 


f_viewDistance_default = 1250;
f_viewDistance_tank = 2000;
f_viewDistance_rotaryWing = 2500;
f_viewDistance_fixedWing = 5000;
[] execVM "f\common\f_addSetViewDistanceEHs.sqf";

// ====================================================================================

// F3 - Authorised Crew Check


// VehicleName addEventhandler ["GetIn", {[_this,[UnitName1,UnitName2]] execVM "f\common\f_isAuthorisedCrew.sqf"}];

// ====================================================================================

// F3 - Authorised Crew Type Check


he1 addEventhandler ["GetIn", {[_this,["B_Helipilot_F"]] execVM "f\common\f_isAuthorisedCrewType.sqf"}];
he2 addEventhandler ["GetIn", {[_this,["B_Helipilot_F"]] execVM "f\common\f_isAuthorisedCrewType.sqf"}];
he3 addEventhandler ["GetIn", {[_this,["B_Helipilot_F"]] execVM "f\common\f_isAuthorisedCrewType.sqf"}];
he4 addEventhandler ["GetIn", {[_this,["B_Helipilot_F"]] execVM "f\common\f_isAuthorisedCrewType.sqf"}];
he5 addEventhandler ["GetIn", {[_this,["B_Helipilot_F"]] execVM "f\common\f_isAuthorisedCrewType.sqf"}];

// ====================================================================================

// F3 - Casualties Cap


// [[GroupName],100,1] execVM "f\server\f_endOnCasualtiesCap.sqf";

// NATO
//[[GrpNO_CO,GrpNO_DC,GrpNO_ASL,GrpNO_BSL,GrpNO_CSL,GrpNO_A1,GrpNO_A2,GrpNO_A3,GrpNO_B1,GrpNO_B2,GrpNO_B3,GrpNO_C1,GrpNO_C2,GrpNO_C3,GrpNO_MMG1,GrpNO_MAT1,GrpNO_ENG1,GrpNO_DT1,GrpNO_TH1,GrpNO_AH1],100,1] execVM "f\server\f_endOnCasualtiesCap.sqf";

// Iran
//[[GrpIR_CO,GrpIR_DC,GrpIR_ASL,GrpIR_BSL,GrpIR_CSL,GrpIR_A1,GrpIR_A2,GrpIR_A3,GrpIR_B1,GrpIR_B2,GrpIR_B3,GrpIR_C1,GrpIR_C2,GrpIR_C3,GrpIR_MMG1,GrpIR_MAT1,GrpIR_ENG1,GrpIR_DT1,GrpIR_TH1,GrpIR_AH1],100,1] execVM "f\server\f_endOnCasualtiesCap.sqf";

// ====================================================================================

// F3 - Casualties Cap (Advanced)


// [[GroupName],100] execVM "f\server\f_casualtiesCapAdv.sqf";

// ====================================================================================

// F3 - AI Skill Selector (coop)


// f_isFriendlyBLU = 1;
// f_isFriendlyRES = 1;
// f_isFriendlyOPF = 0;
// f_isFriendlyCIV = 1;
// [] execVM "f\common\f_setAISkill.sqf";

// ====================================================================================

// F3 - AI Skill Selector (A&D)


// f_isFriendlyToBLU_RES = 1;
// f_isFriendlyToBLU_CIV = 1;
// [] execVM "f\common\f_setAISkillAD.sqf";

// ====================================================================================

// F3 - Construction Interface (COIN) Presets


// f_COINstopped = false;
// [COINName,"UnitName",0,2500] execVM "f\common\f_COINpresets.sqf";

// ====================================================================================

// F3 - Name Tags


[] execVM "f\common\f_recog\recog_init.sqf";

// ====================================================================================

// F3 - Group E&E Check


// [GroupName,ObjectName,100,1] execVM "f\server\f_groupEandECheck.sqf";

// ====================================================================================

// F3 - ORBAT Notes


[] execVM "f\common\f_orbatNotes.sqf";

// ====================================================================================

// F3 - Tripwire action


// [[UnitName1,UnitName2],25,2,east] execVM "f\common\fa_tripwire_init.sqf";

// ====================================================================================

// F3 - Join Group Action


[false] execVM "f\common\f_groupJoinAddOption.sqf";

// ====================================================================================

// DWS - Mission scripts

_nul = [] execVM "scripts\enemycount.sqf";
_nul = [] execVM "scripts\antishoot.sqf";
//admin actions
[player] call compile preprocessFileLineNumbers "adminActions\main.sqf";
//Init UPSMON scritp (must be run on all clients)
call compile preprocessFileLineNumbers "scripts\Init_UPSMON.sqf";	
//Process statements stored using setVehicleInit
processInitCommands;
//Finish world initialization before mission is launched. 
finishMissionInit;
//BTC Revive init
call compile preprocessFile "=BTC=_revive\=BTC=_revive_init.sqf";
// If your mission uses the event handler killed please add your code at the end of this file: "TeamKillerLock\TKL_Killed.sqf"
TKL_Killed = Compile preprocessFile "TeamKillerLock\TKL_Killed.sqf";

// null = [MaximumNumberOfTeamKills,TeamKillRemoverTime] ExecVM "TeamKillerLock\TKL_Manager.sqf";
// MaximumNumberOfTeamKills
// Maximum number of team kills (Default is 4)
// Once a player record reach this value the game will be locked.

// TeamKillRemoverTime
// Time after which one team kill will be removed from the team kill record (Default is 480s -> 8min)
null = [4,600] ExecVM "TeamKillerLock\TKL_Manager.sqf";
//CEP Unit Caching
// 1000m would be the default minimum
[1000] call compile preprocessFileLineNumbers "modules\CEP_caching\main.sqf";

// ====================================================================================

