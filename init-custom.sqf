if ((!isServer) && (player != player)) then
{
  waitUntil {player == player};
};
//admin actions
[player] call compile preprocessFileLineNumbers "adminActions\main.sqf";
//Init UPSMON scritp (must be run on all clients)
call compile preprocessFileLineNumbers "scripts\Init_UPSMON.sqf";	
//Process statements stored using setVehicleInit
processInitCommands;
//Finish world initialization before mission is launched. 
finishMissionInit;
//BTC Revive init
call compile preprocessFile "=BTC=_revive\=BTC=_revive_init.sqf";
// If your mission uses the event handler killed please add your code at the end of this file: "TeamKillerLock\TKL_Killed.sqf"
TKL_Killed = Compile preprocessFile "TeamKillerLock\TKL_Killed.sqf";

// null = [MaximumNumberOfTeamKills,TeamKillRemoverTime] ExecVM "TeamKillerLock\TKL_Manager.sqf";

// MaximumNumberOfTeamKills
// Maximum number of team kills (Default is 4)
// Once a player record reach this value the game will be locked.

// TeamKillRemoverTime
// Time after which one team kill will be removed from the team kill record (Default is 480s -> 8min)
null = [4,600] ExecVM "TeamKillerLock\TKL_Manager.sqf";
//Persistant gear menu init
//Should make markers invisible
"area0" setMarkerAlpha 0;
"area1" setMarkerAlpha 0;
"area2" setMarkerAlpha 0;
"area3" setMarkerAlpha 0;
"area4" setMarkerAlpha 0;
"area5" setMarkerAlpha 0;
"area6" setMarkerAlpha 0;
"area7" setMarkerAlpha 0;
"area8" setMarkerAlpha 0;
"area9" setMarkerAlpha 0;
"area10" setMarkerAlpha 0;
"area11" setMarkerAlpha 0;
"area12" setMarkerAlpha 0;
"area13" setMarkerAlpha 0;
"area14" setMarkerAlpha 0;
"area15" setMarkerAlpha 0;
"area16" setMarkerAlpha 0;
"area17" setMarkerAlpha 0;
//How many enemies left hint
if(isServer) then
{
	[] execFSM "cleanup.fsm";
};
_nul = [] execVM "scripts\enemycount.sqf";
_nul = [] execVM "scripts\antishoot.sqf";