private["_key","_ctrl"];
_key = _this select 1;
_ctrl = _this select 3;
_handled = false;

//SEAGULL FIX (PRESS LCTRL + O)
if(_key == 24 && _ctrl) then
{
	selectPlayer player;
	titleText["You are no longer a bird.","PLAIN"];
};