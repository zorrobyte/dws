/*
Created by =BTC= Giallustio
version 0.3
Visit us at: 
http://www.blacktemplars.altervista.org/
06/03/2012
*/

////////////////// EDITABLE \\\\\\\\\\\\\\\\\\\\\\\\\\
BTC_revive_time_min = 5;
BTC_revive_time_max = 600;
BTC_who_can_revive  = ["Man"];
BTC_black_screen    = 0;//Black screen + button while unconscious or action wheel and clear view
BTC_active_mobile   = 1;//Active mobile respawn (You have to put in map the vehicle and give it a name. Then you have to add one object per side to move to the mobile (BTC_base_flag_west,BTC_base_flag_east) - (1 = yes, 0 = no))
BTC_mobile_respawn  = 1;//Active the mobile respawn fnc (1 = yes, 0 = no)
BTC_mobile_respawn_time = 30;//Secs delay for mobile vehicle to respawn
BTC_need_first_aid = 0;//You need a first aid kit to revive (1 = yes, 0 = no)
BTC_pvp = 0; //(disable the revive option for the enemy)
BTC_vehs_mobile_west = [mobile_west_0];//Editable - define mobile west
BTC_vehs_mobile_east = [mobile_east_0];//Editable - define mobile east
////////////////// Don't edit below \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//FNC
call compile preprocessFile "=BTC=_revive\=BTC=_functions.sqf";

if (isServer) then
{
	//Mobile
	if (BTC_active_mobile == 1 && count BTC_vehs_mobile_west != 0) then {for "_i" from 0 to ((count BTC_vehs_mobile_west) - 1) do {_veh = (BTC_vehs_mobile_west select _i);_var = format ["mobile_%1_%2",west,_i];_veh setVariable ["BTC_mobile",_var,true];if (BTC_mobile_respawn == 1) then {_resp = [_veh,_var] spawn BTC_vehicle_mobile_respawn;}};;} else {{deleteVehicle _x} foreach BTC_vehs_mobile_west;};
	if (BTC_active_mobile == 1 && count BTC_vehs_mobile_east != 0) then {for "_i" from 0 to ((count BTC_vehs_mobile_east) - 1) do {_veh = (BTC_vehs_mobile_east select _i);_var = format ["mobile_%1_%2",east,_i];_veh setVariable ["BTC_mobile",_var,true];if (BTC_mobile_respawn == 1) then {_resp = [_veh,_var] spawn BTC_vehicle_mobile_respawn;};};} else {{deleteVehicle _x} foreach BTC_vehs_mobile_east;};
};
if (isDedicated) exitWith {};

BTC_dragging = false;
//Init
[] spawn
{
	waitUntil {!isNull player};
	waitUntil {player == player};
	//"BTC_PVEH" addPublicVariableEventHandler BTC_fnc_PVEH;
	BTC_side = side player;
	BTC_respawn_marker = format ["respawn_%1",side player];
	player addEventHandler ["HandleDamage", BTC_fnc_handledamage];
	player addEventHandler ["Killed", BTC_player_killed];
	player setVariable ["BTC_need_revive",0,true];
	if ([player] call BTC_can_revive) then {player addAction [("<t color=""#ED2744"">") + ("First aid") + "</t>","=BTC=_revive\=BTC=_addAction.sqf",[[],BTC_first_aid], 8, true, true, "", "[] call BTC_check_action_first_aid"];};
	player addAction [("<t color=""#ED2744"">") + ("Drag") + "</t>","=BTC=_revive\=BTC=_addAction.sqf",[[],BTC_drag], 8, true, true, "", "[] call BTC_check_action_drag"];
	if (BTC_active_mobile == 1) then {if (side player == west) then {{_spawn = [format ["%1",_x]] spawn BTC_mobile_marker;BTC_base_flag_west addAction [("<t color=""#ED2744"">") + ("Move to mobile " + format ["%1",_x]) + "</t>","=BTC=_revive\=BTC=_addAction.sqf",[[str (_x)],BTC_move_to_mobile], 8, true, true, "", format ["[str (%1)] call BTC_mobile_check",_x]];} foreach BTC_vehs_mobile_west;} else {{_spawn = [format ["%1",_x]] spawn BTC_mobile_marker;BTC_base_flag_east addAction [("<t color=""#ED2744"">") + ("Move to mobile " + format ["%1",_x]) + "</t>","=BTC=_revive\=BTC=_addAction.sqf",[[str (_x)],BTC_move_to_mobile], 8, true, true, "", format ["[str (%1)] call BTC_mobile_check",_x]];} foreach BTC_vehs_mobile_east;};};
	BTC_weapons = weapons player;//diag_log text format ["%1",BTC_weapons];
BTC_prim_weap = primaryWeapon player;//diag_log text format ["%1",BTC_prim_weap];
BTC_prim_items = primaryWeaponItems player;//diag_log text format ["%1",BTC_prim_items];
BTC_sec_weap = secondaryWeapon player;//diag_log text format ["%1",BTC_sec_weap];
BTC_sec_items = secondaryWeaponItems player;//diag_log text format ["%1",BTC_sec_items];
BTC_items_assigned = assignedItems player;//diag_log text format ["%1",BTC_items_assigned];
BTC_items = items player;//diag_log text format ["%1",BTC_items];
//_handgun = handgun player;
BTC_handgun_items = handgunItems player;//diag_log text format ["%1",BTC_handgun_items];
BTC_mags = magazines player;//diag_log text format ["%1",BTC_mags];
BTC_goggles = goggles player;//diag_log text format ["%1",BTC_goggles];
BTC_headgear = headgear player;//diag_log text format ["%1",BTC_headgear];
BTC_uniform = uniform player;//diag_log text format ["%1",BTC_uniform];
BTC_uniform_items = uniformItems player;//diag_log text format ["%1",BTC_uniform_items];
BTC_vest = vest player;//diag_log text format ["%1",BTC_vest];
BTC_vest_items = vestItems player;//diag_log text format ["%1",BTC_vest_items];
BTC_back_pack = backpack player;//diag_log text format ["%1",BTC_back_pack];
BTC_weap_sel = currentWeapon player;//diag_log text format ["%1",BTC_weap_sel];
	hint "REVIVE STARTED";
};