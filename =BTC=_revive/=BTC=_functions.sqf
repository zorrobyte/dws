/*
Created by =BTC= Giallustio
version 0.3
Visit us at: 
http://www.blacktemplars.altervista.org/
06/03/2012
*/
//Functions
BTC_fnc_handledamage =
{
	_player = _this select 0;
	_enemy  = _this select 3;
	_damage = _this select 2;
	_part   = _this select 1;
	if (Alive _player) then
	{
		BTC_weapons = weapons _player;//diag_log text format ["%1",BTC_weapons];
		BTC_prim_weap = primaryWeapon _player;//diag_log text format ["%1",BTC_prim_weap];
		BTC_prim_items = primaryWeaponItems _player;//diag_log text format ["%1",BTC_prim_items];
		BTC_sec_weap = secondaryWeapon _player;//diag_log text format ["%1",BTC_sec_weap];
		BTC_sec_items = secondaryWeaponItems _player;//diag_log text format ["%1",BTC_sec_items];
		BTC_items_assigned = assignedItems _player;//diag_log text format ["%1",BTC_items_assigned];
		BTC_items = items _player;//diag_log text format ["%1",BTC_items];
		//_handgun = handgun player;
		BTC_handgun_items = handgunItems _player;//diag_log text format ["%1",BTC_handgun_items];
		BTC_mags = magazines _player;//diag_log text format ["%1",BTC_mags];
		BTC_goggles = goggles _player;//diag_log text format ["%1",BTC_goggles];
		BTC_headgear = headgear _player;//diag_log text format ["%1",BTC_headgear];
		BTC_uniform = uniform _player;//diag_log text format ["%1",BTC_uniform];
		BTC_uniform_items = uniformItems _player;//diag_log text format ["%1",BTC_uniform_items];
		BTC_vest = vest _player;//diag_log text format ["%1",BTC_vest];
		BTC_vest_items = vestItems _player;//diag_log text format ["%1",BTC_vest_items];
		BTC_back_pack = backpack _player;//diag_log text format ["%1",BTC_back_pack];
		BTC_weap_sel = currentWeapon _player;//diag_log text format ["%1",BTC_weap_sel];
	};
	_damage
	//if (format ["%1", _player getVariable "BTC_need_revive"] == "1") then {} else {_damage};
};
BTC_fnc_PVEH =
{
	//0 - first aid - create // [0,east,pos]
	//1 - first aid - delete
	_array = _this select 1;
	_type  = _array select 0;
	switch (true) do
	{
		case (_type == 0) : 
		{
			_side = _array select 1;
			_unit = _array select 3;
			_side = _array select 1;
			if (_side == side player) then 
			{
				_pos = _array select 2;
				_marker = createmarkerLocal [format ["FA_%1", _pos], _pos];
				format ["FA_%1", _pos] setmarkertypelocal "mil_box";
				format ["FA_%1", _pos] setMarkerTextLocal "F.A.";
				format ["FA_%1", _pos] setmarkerColorlocal "ColorGreen";
				format ["FA_%1", _pos] setMarkerSizeLocal [0.3, 0.3];
				[_pos,_unit] spawn
				{
					_pos = _this select 0;
					_unit = _this select 1;
					sleep 2;
					WaitUntil {((isNull _unit) || (format ["%1", player getVariable "BTC_need_revive"] == "0"))};
					deleteMarker format ["FA_%1", _pos];
				};
			};
		};
		case (_type == 1) : {(array select 1) setDir 180;};
	};
};
BTC_first_aid =
{
	private ["_injured"];
	_men = nearestObjects [player, ["Man"], 2];
	if (count _men > 1) then {_injured = _men select 1;};
	if (format ["%1",_injured getVariable "BTC_need_revive"] != "1") exitWith {};
	_array_item = items player;
	if ((_array_item find "FirstAidKit" == -1) && (BTC_need_first_aid == 1)) exitWith {};
	if (BTC_need_first_aid == 1) then {player removeItem "FirstAidKit";};
	player playMove "AinvPknlMstpSlayWrflDnon_medic";
	sleep 5;
	waitUntil {!Alive player || (animationState player != "AinvPknlMstpSlayWrflDnon_medic" && animationState player != "amovpercmstpsraswrfldnon_amovpknlmstpsraswrfldnon" && animationState player != "amovpknlmstpsraswrfldnon_ainvpknlmstpslaywrfldnon" && animationState player != "ainvpknlmstpslaywrfldnon_amovpknlmstpsraswrfldnon")};
	if (Alive player && Alive _injured) then
	{
		_injured setVariable ["BTC_need_revive",0,true];
		_injured playMoveNow "AinjPpneMstpSnonWrflDnon_rolltoback";
	};
};
BTC_drag =
{
	private ["_injured"];
	_men = nearestObjects [player, ["Man"], 2];
	if (count _men > 1) then {_injured = _men select 1;};
	if (format ["%1",_injured getVariable "BTC_need_revive"] != "1") exitWith {};
	BTC_dragging = true;
	_injured attachTo [player, [0, 1.1, 0.092]];
	player playMoveNow "AcinPknlMstpSrasWrflDnon";
	_id = player addAction [("<t color=""#ED2744"">") + ("Release") + "</t>","=BTC=_revive\=BTC=_addAction.sqf",[[],BTC_release], 9, true, true, "", "true"];
	_injured playMoveNow "AinjPpneMstpSnonWrflDb_grab";
	_injured setVehicleInit "this setDir 180;";processInitCommands;
	//BTC_PVEH = [1,_injured];publicVariable "BTC_PVEH";
	WaitUntil {!Alive player || ((animationstate player == "acinpknlmstpsraswrfldnon") || (animationstate player == "acinpknlmwlksraswrfldb"))};
	while {!isNull player && alive player && !isNull _injured && alive _injured && format ["%1", _injured getVariable "BTC_need_revive"] == "1" && BTC_dragging} do
	{
		sleep 0.1;
	};
	detach _injured;
	if (format ["%1",_injured getVariable "BTC_need_revive"] == "1") then {_injured playMoveNow "AinjPpneMstpSnonWrflDb_release";};
	player removeAction _id;
	BTC_dragging = false;
};
BTC_release =
{
	BTC_dragging = false;
	player playMoveNow "AmovPknlMstpSrasWrflDnon";//AmovPpneMstpSrasWrflDnon
};
BTC_player_killed =
{
	private ["_type_backpack","_weapons","_magazines","_weapon_backpack","_ammo_backpack","_score","_score_array","_name","_body_marker"];
	if (BTC_black_screen == 1) then {titleText ["", "BLACK OUT"];};
	_body = _this select 0;
	[_body] spawn
	{
		_body = _this select 0;
		_body removeEventHandler["fired",customEVH];
		_dir = getDir _body;
		_pos = getPosATL vehicle _body;
		WaitUntil {Alive player};
		if (BTC_pvp == 0) then {player setcaptive true;};
		player setvehicleInit "this allowDamage false;";ProcessInitCommands;
		player setVariable ["BTC_need_revive",1,true];
		player switchMove "AinjPpneMstpSnonWrflDnon";
		if ([player] call BTC_can_revive) then {player addAction [("<t color=""#ED2744"">") + ("First aid") + "</t>","=BTC=_revive\=BTC=_addAction.sqf",[[],BTC_first_aid], 8, true, true, "", "[] call BTC_check_action_first_aid"];};
		player addAction [("<t color=""#ED2744"">") + ("Drag") + "</t>","=BTC=_revive\=BTC=_addAction.sqf",[[],BTC_drag], 8, true, true, "", "[] call BTC_check_action_drag"];
		removeAllweapons player;
		removeuniform player;
		removevest player;
		removeheadgear player;
		removegoggles player;
		removeBackPack player;
		{player removeItem _x} foreach (items player);
		{player unassignItem _x;player removeItem _x} foreach (assignedItems player);
		if (BTC_uniform != "") then {player addUniform BTC_uniform;};
		if (BTC_vest != "") then {player addVest BTC_vest;};
		if (BTC_back_pack != "") then {player addBackPack BTC_back_pack;clearAllItemsFromBackpack player;};
		if (BTC_goggles != "") then {player addGoggles BTC_goggles;};
		if (BTC_headgear != "") then {player addHeadgear BTC_headgear;};
		if (count BTC_mags > 0) then {{player addMagazine _x;} foreach BTC_mags;};
		if (count BTC_items_assigned > 0) then {{player addItem _x;player assignItem _x} foreach BTC_items_assigned;};
		if (count BTC_items > 0) then {{player addItem _x;} foreach BTC_items;};
		if (count BTC_weapons > 0) then {{player addweapon _x} foreach BTC_weapons;};
		{player removeItemFromPrimaryWeapon _x} foreach (primaryWeaponItems player);
		if (count BTC_prim_items > 0) then {{player addPrimaryWeaponItem _x;} foreach BTC_prim_items;};
		if (count BTC_sec_items > 0) then {{player addSecondaryWeaponItem _x;} foreach BTC_sec_items;};
		if (count BTC_handgun_items > 0) then {{player addHandgunItem _x;} foreach BTC_handgun_items;};
		player selectweapon BTC_weap_sel;

		WaitUntil {animationstate player == "ainjppnemstpsnonwrfldnon"};
		sleep 2;
		player setDir _dir;
		player setPosATL [_pos select 0, _pos select 1, 0.1];
		deletevehicle _body;
		_body_marker = player;
		_side = side player;
		_injured = player;
		//if marker
		//BTC_PVEH = [0,_side,_pos,_body_marker];publicVariable "BTC_PVEH";
		disableUserInput true;
		for [{_n = BTC_revive_time_min}, {_n != 0 && damage player > 0.2}, {_n = _n - 0.5}] do
		{
			if (BTC_black_screen == 1) then {titleText ["", "BLACK FADED"];};
			sleep 0.5;
		};
		disableUserInput false;
		_time = time;
		_timeout = _time + BTC_revive_time_max;
		private ["_id"];
		if (BTC_black_screen == 0) then {_id = player addAction [("<t color=""#ED2744"">") + ("Respawn") + "</t>","=BTC=_revive\=BTC=_addAction.sqf",[[],BTC_player_respawn], 9, true, true, "", "true"];};
		if (BTC_black_screen == 1) then {_dlg = createDialog "BTC_respawn_button_dialog";};
		while {format ["%1", player getVariable "BTC_need_revive"] == "1" && time < _timeout} do
		{
			if (BTC_black_screen == 0) then {if (animationstate player != "ainjppnemstpsnonwrfldnon") then {player switchMove "AinjPpneMstpSnonWrflDnon";};};
			if (BTC_black_screen == 1) then {if (!Dialog) then {_dlg = createDialog "BTC_respawn_button_dialog";};};
			_healer = call BTC_check_healer;
			if (BTC_black_screen == 1) then {titleText [format ["%1\n%2", round (_timeout - time),_healer], "BLACK FADED"];} else {hintSilent format ["%1\n%2", round (_timeout - time),_healer];};
			sleep 0.5;
		};
		closedialog 0;
		if (time > _timeout && format ["%1", player getVariable "BTC_need_revive"] == "1") then 
		{
			_respawn = [] spawn BTC_player_respawn;
		};
		if (format ["%1", player getVariable "BTC_need_revive"] == "0") then 
		{
			if (BTC_black_screen == 1) then {titleText ["", "BLACK IN"];} else {hintSilent "";};
			player playMove "amovppnemstpsraswrfldnon";
			player playMove "";
		};
		if (BTC_black_screen == 0) then {player removeAction _id;};
		if (BTC_pvp == 0) then {player setcaptive false;};
		player setvehicleInit "this allowDamage true;";ProcessInitCommands;
		hintSilent "";
	};
};
BTC_check_healer =
{
	_pos = getpos player;
	_men = [];_dist = 501;_healer = objNull;_healers = [];
	_msg = "No healer in 500 m";
	_men = nearestObjects [_pos, BTC_who_can_revive, 500];
	if (count _men > 0) then
	{
		{if (Alive _x && format ["%1",_x getVariable "BTC_need_revive"] != "1" && ([_x] call BTC_can_revive) && isPlayer _x && side _x == BTC_side) then {_healers = _healers + [_x];};} foreach _men;
		if (count _healers > 0) then
		{
			{
				if (_x distance _pos < _dist) then {_healer = _x;_dist = _x distance _pos;};
			} foreach _healers;
			if !(isNull _healer) then {_msg = format ["%1 could heal you! He is %2 m away!", name _healer,round(_healer distance _pos)];};
		};
	};
	_msg
};
BTC_player_respawn =
{
	player setVariable ["BTC_need_revive",0,true];
	player setPos getMarkerPos BTC_respawn_marker;
	sleep 1;
	player setDamage 0;
	player switchMove "amovpercmstpslowwrfldnon";
	player switchMove "";
	player enableSimulation false;
	player enableSimulation true;
	if (BTC_black_screen == 1) then {titleText ["", "BLACK IN"];};
};
BTC_check_action_first_aid =
{
	_cond = false;
	_array_item = items player;
	_men = nearestObjects [player, ["Man"], 2];
	if (count _men > 1) then
	{
		if (format ["%1", (_men select 1) getVariable "BTC_need_revive"] == "1" && !BTC_dragging) then {_cond = true;};
	};
	if (_cond && BTC_pvp == 1) then 
	{
		if (side (_men select 1) == BTC_side) then {_cond = true;} else {_cond = false;};
	};
	if (_cond && BTC_need_first_aid == 1) then
	{
		
		if (_array_item find "FirstAidKit" == -1) then {_cond = false;};
	};
	_cond
};
BTC_check_action_drag =
{
	_cond = false;
	_men = nearestObjects [player, ["Man"], 2];
	if (count _men > 1) then
	{
		if (format ["%1", (_men select 1) getVariable "BTC_need_revive"] == "1" && !BTC_dragging) then {_cond = true;};
	};
	_cond
};
BTC_can_revive =
{
	_unit = _this select 0;
	_array_item = items _unit;
	_cond = false;
	{if (_unit isKindOf _x) then {_cond = true};} foreach BTC_who_can_revive;
	if (_cond && BTC_need_first_aid == 1) then
	{
		
		if (_array_item find "FirstAidKit" == -1) then {_cond = false;};
	};
	_cond
};
//Mobile
BTC_move_to_mobile =
{
	_var = _this select 0;
	_mobile = objNull;
	{
		if ((typeName (_x getvariable "BTC_mobile")) == "STRING") then
		{
			if ((_x getvariable "BTC_mobile") == _var) then {_mobile = _x;};
		};
	} foreach vehicles;
	if (isNull _mobile) exitWith {};
	if (speed _mobile > 2) exitWith {hint "Mobile respawn is moving! Can't move there!";};
	_pos = getPos _mobile;
	titleText ["Get Ready", "BLACK OUT"];
	sleep 3;
	titleText ["Get Ready", "BLACK FADED"];
	sleep 2;
	titleText ["", "BLACK IN"];
	player setPos [(_pos select 0) + ((random 50) - (random 50)), (_pos select 1) + ((random 50) - (random 50)), 0];
};
BTC_mobile_marker =
{
	_var = _this select 0;
	while {true} do
	{
		_obj = objNull;
		while {isNull _obj} do 
		{
			{
				if (format ["%1",_x getVariable "BTC_mobile"] == _var && Alive _x) then {_obj = _x;};
			} foreach vehicles;
			sleep 1;
		};
		deleteMarkerLocal format ["%1", _var];
		_marker = createmarkerLocal [format ["%1", _var], getPos _obj];
		format ["%1", _var] setmarkertypelocal "mil_dot";
		format ["%1", _var] setMarkerTextLocal format ["%1", _var];
		format ["%1", _var] setmarkerColorlocal "ColorGreen";
		format ["%1", _var] setMarkerSizeLocal [0.5, 0.5];
		hint format ["%1 is available!", _var];
		while {Alive _obj} do
		{
			format ["%1", _var] setMarkerPosLocal (getPos _obj);
			if (speed _obj < 2) then {format ["%1", _var] setMarkerTextLocal format ["%1 deployed", _var];format ["%1", _var] setmarkerColorlocal "ColorGreen";} else {format ["%1", _var] setMarkerTextLocal format ["%1 is moving", _var];format ["%1", _var] setmarkerColorlocal "ColorBlack";};
			sleep 1;
		};
		hint format ["%1 has been destroyed!", _var];
		format ["%1", _var] setMarkerTextLocal format ["%1 destroyed!", _var];
		format ["%1", _var] setmarkerColorlocal "ColorRed";
		if (BTC_mobile_respawn == 0) exitWith {};
	};
};
BTC_mobile_check =
{
	_var = _this select 0;
	_cond = false;
	{
		if ((typeName (_x getvariable "BTC_mobile")) == "STRING") then
		{
			if ((_x getvariable "BTC_mobile") == _var) then {_cond = true;};
		};
	} foreach vehicles;
	_cond
};
BTC_vehicle_mobile_respawn =
{
	_veh  = _this select 0;
	_var  = _this select 1;
	_type = typeOf _veh;
	_pos  = getPos _veh;
	_dir  = getDir _veh;
	waitUntil {sleep 1; !Alive _veh};
	_veh setVariable ["mobile",0,true];
	sleep BTC_mobile_respawn_time;
	_veh  = _type createVehicle _pos;
	_veh setDir _dir;
	_veh setVelocity [0, 0, -1];
	_veh setVariable ["BTC_mobile",_var,true];
	_resp = [_veh,_var] spawn BTC_vehicle_mobile_respawn;
};