MISSION README
==============

Mission: Dynamic War Sandbox
Version: v24
Developer: TAW_Zorrobyte
Description: Seize high value locations within Stratis in preparation for seige on Atlas. 



README CONTENTS
===============

01. VERSION HISTORY
02. COPYRIGHT STATEMENT
03. TERMS OF USE
04. LEGAL DISCLAIMER
05. INSTALLATION
06. REQUIRED ADDONS
07. NOTES
08. CHANGE HISTORY



01. VERSION HISTORY
===================

Version | Date | Notes

v23 | 3/13/2013 | Rewrote DWS from the ground up
v24 | 3/13/2013 | Reduced AI in marina, was lagging clientside fps maybe due to high polycount
v25 | 3/14/2013 | Added caching module and HC is now optional depending if HC is in slot



02. COPYRIGHT STATEMENT
=======================

This mission is (c)2013 TAW_Zorrobyte. All rights reserved.



03. TERMS OF USE
================

This mission (hereafter 'Software') contains files to be used in the PC CD-ROM simulator "Arma 3" (hereafter 'ArmA3'). To use the Software you must agree to the following conditions of use:

1. TAW_Zorrobyte (hereafter 'The Author') grant to you a personal, non-exclusive license to use the Software.

2. The commercial exploitation of the Software without written permission from The Author(s) is expressly prohibited.

3. This Software is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US).



04. LEGAL DISCLAIMER
====================

The Software is distributed without any warranty; without even the implied warranty of merchantability or fitness for a particular purpose. The Software is not an official addon or tool. Use of the Software (in whole or in part) is entirely at your own risk.



05. INSTALLATION
================

To begin using the Software:

1. Move the file DWS_XX_XX.pbo into the following directory:

c:\Program Files (x86)\Steam\steamapps\common\Arma 3\MPMissions



06. REQUIRED ADDONS
===================

To play this mission the following addons are required:

None


07. NOTES
=========

Report bugs and feature requests @ https://bitbucket.org/zorrobyte/dws/issues



08. CHANGE HISTORY
==================

Version | Date

v23 | 3/13/2013
v24 | 3/13/2013
v25 | 3/14/2013