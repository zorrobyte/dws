
_Killer = _This Select 0;
_Punishment = _This Select 1;
_LinkedToPlayer = _This Select 2;
Punish_YES = False;
_n = 15;

If (_LinkedToPlayer) Then
	{
	WaitUntil {Alive(Player);};
	Sleep 1;
	_Act = (Vehicle Player) AddAction ['<t color="#00FF00">' + Format ["FORGIVE KILLER: %1",Name _Killer] + '</t>', "TeamKillerLock\TKL_VoteAction_Punish.sqf",[],25,false];
	While {!Punish_YES And (_n > 0)} Do
		{
		TitleText [Format["\n\n\n\nFORGIVE KILLER?\n(Use action menu)\n%1",_n],"Plain"];
		Sleep 1;
		_n = _n - 1;
		};
	TitleText ["","Plain"];
	(Vehicle Player) RemoveAction _Act;
	};

If (!Punish_YES) Then {((GetPlayerUID _Killer)+"_Side") SetMarkerPos [(GetMarkerPos ((GetPlayerUID _Killer)+"_Side") Select 0)+_Punishment,0];};


